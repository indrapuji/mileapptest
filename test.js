// Frontend Developer

// 1. Do you prefer vuejs or reactjs? Why ?
// i prefer use reactjs, because reactjs Provides more flexibility when developing larger applications and complex

// 2. What complex things have you done in frontend development ?
// I don't know for sure, but create a web with a lots of rest api on one page (e-commerce)

// 3. why does a UI Developer need to know and understand UX? how far do you understand it?
// to make our web easy to use, and people confort to stay in our web

// 4. Give your analysis results regarding https://taskdev.mile.app/login from the UI / UX side!
// simple but powerfull

// 5. Create a better login page based on https://taskdev.mile.app/login and deploy on https://www.netlify.com (https://www.netlify.com/)!
// https://5fa1d44ae494c800db444f46--condescending-wozniak-a32e86.netlify.app/

// 6. Solve the logic problems below !

let A = 3;
let B = 5;

B = [A, (A = B)][0];
console.log(`6.a Swap the values of variables A and B`);
console.log(`A = ${A}`);
console.log(`B = ${B}`);
console.log(``);

let numbers = [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100];
let missingNumber = numbers.reduce(function (acc, cur, ind, arr) {
  let diff = cur - arr[ind - 1];
  if (diff > 1) {
    let i = 1;
    while (i < diff) {
      acc.push(arr[ind - 1] + i);
      i++;
    }
  }
  return acc;
}, []);
console.log(`6.b Find the missing numbers from 1 to 100`);
console.log(missingNumber);
console.log(``);

function duplicate(array) {
  let object = {};
  let result = [];

  array.forEach(function (item) {
    if (!object[item]) object[item] = 0;
    object[item] += 1;
  });

  for (let prop in object) {
    if (object[prop] >= 2) {
      result.push(Number(prop));
    }
  }

  return result;
}

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25, 25, 25, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 34, 34, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 83, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 92, 93, 94, 95, 96, 97, 98, 99, 100];
console.log(`6.c return the number which is called more than 1`);
console.log(duplicate(numbers));
console.log(``);


array_code = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."]
let result = {}
array_code.forEach((data) => {
  let splitted = data.split(".");
  let temp = [];
  splitted.forEach((data, idx) => {
    if (idx !== splitted.length - 1) {
      temp.push([data]);
    }
  });
  if (temp.length === 1) {
    result[temp[0]] = {};
  } else if (temp.length === 2) {
    result[temp[0]][temp[1]] = {};
  } else if (temp.length === 3) {
    result[temp[0]][temp[1]][temp[2]] = data;
  }
});
console.log(`6.d `);
console.log(result);
