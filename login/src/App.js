import React from "react";
import { Image, Form, Button, Container } from "react-bootstrap";
import Logo from "./assets/logo.png";
import "./App.css";

export default function App() {
  return (
    <>
      <Container>
        <div className="loginpage">
          <div className="logo">
            <Image src={Logo} style={{ height: 80 }} />
            <p className="title">
              Your one stop platform to manage all of your <br /> field service management
            </p>
          </div>
          <Form className="loginForm">
            <Form.Group>
              <Form.Control type="user" placeholder="Enter your organization's name" />
            </Form.Group>
            <Button variant="primary" block>
              Login
            </Button>
            <p className="register">
              Not registered yet? <span className="contact">Contact us</span> for more info
            </p>
          </Form>
        </div>
      </Container>
    </>
  );
}
